resource "openstack_blockstorage_volume_v3" "control_volumes_b" {
  count = var.control_node_volumes_count != "" ? var.control_node_volumes_count : var.control_nodes_count
  name  = "${var.infra_name}-control-node-volume-b-${count.index+1}"
  size  = var.control_nodes_extra_volume_size
}

resource "openstack_blockstorage_volume_v3" "control_volumes_c" {
  count = var.control_node_volumes_count != "" ? var.control_node_volumes_count : var.control_nodes_count
  name  = "${var.infra_name}-control-node-volume-c-${count.index+1}"
  size  = var.control_nodes_extra_volume_size
}


resource "openstack_compute_volume_attach_v2" "control_volumes_b_attachments" {
  count = var.control_node_volumes_count != "" ? var.control_node_volumes_count : var.control_nodes_count
  instance_id = element(openstack_compute_instance_v2.control_nodes.*.id, count.index)
  volume_id   = element(openstack_blockstorage_volume_v3.control_volumes_b.*.id, count.index)
  #device = "/dev/sdb"
}

resource "openstack_compute_volume_attach_v2" "control_volumes_c_attachments" {
  count = var.control_node_volumes_count != "" ? var.control_node_volumes_count : var.control_nodes_count
  instance_id = element(openstack_compute_instance_v2.control_nodes.*.id, count.index)
  volume_id   = element(openstack_blockstorage_volume_v3.control_volumes_c.*.id, count.index)
  #device = "/dev/sdc"
}
