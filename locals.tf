locals {
  all_instances = flatten([
    for group in var.worker_nodes : [
      for i in range(group.count) : {
        name        = "${var.infra_name}-${group.name}-${i + 1}"
        flavor      = group.flavor
        volume_size = group.volume_size
        ip_address  = format("%s.%d", regex("[0-9]+\\.[0-9]+\\.[0-9]+", var.internal_network_cidr), 21 + i)
      }
    ]
  ])
}
