users:
  - default
  - name: ubuntu
    shell: /bin/bash
write_files:
  - path: /root/infra-config-minimal.sh
    content: |
      #!/usr/bin/env bash
      #
      # infra-config-minimal.sh initialization
      #

      # variables
      ICM_DIR="/root/icm"
      declare -A STEP_IP_MAPPING
      all_nodes_except_bastion="10.0.0.(1[1-9]|[2-9][0-9])"
      STEP_IP_MAPPING[step_99_reboot]="${all_nodes_except_bastion}"
      STEP_IP_MAPPING[step_20_disable_firewall]="${all_nodes_except_bastion}"


      # functions

      function get_main_ip_address() {
        ip r l | grep ^default | grep -Eo 'src[ \t]+[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+'
      }

      function is_step_allowed() {
        local step_name="$1"
        local ip="$(get_main_ip_address)"

        if [ -f "${ICM_DIR}/${i_step}.log" -a -s "${ICM_DIR}/${i_step}.log" ]; then
          return 2
        fi
        if [ -n "${STEP_IP_MAPPING[${step_name}]}" ]; then
          if $(echo "${ip}" | grep -Eq "${STEP_IP_MAPPING[${step_name}]}"); then
            return 0
          fi
          return 1
        else
          return 0
        fi
      }


      function step_10_upgrade() {
        apt -y update
        apt -y upgrade
      }

      function step_20_disable_firewall() {
        systemctl disable ufw.service
        systemctl stop ufw.service
      }

      function step_25_systemd_timesyncd() {
        systemctl enable systemd-timesyncd
        systemctl restart systemd-timesyncd
        timedatectl
      }

      function step_30_sysctl() {
        local sysctl_items="net.ipv6.conf.all.disable_ipv6=0 net.ipv6.conf.default.disable_ipv6=1 net.ipv6.conf.all.forwarding=1 net.ipv4.conf.all.forwarding=1 fs.inotify.max_user_instances=512"
        for i_sysctl_id in ${sysctl_items}; do
          sysctl -w ${i_sysctl_id}
          if ! grep -q "^${i_sysctl_id}" /etc/sysctl.conf; then
            echo "${i_sysctl_id}" >> /etc/sysctl.conf
          fi
        done
      }

      function step_99_reboot() {
        reboot
      }

      mkdir -p "${ICM_DIR}"

      for i_step in $(grep "^function step_" $0 | grep -Eo "step_[a-z0-9_]+"); do
        echo "Entering step ${i_step} (dur: ${SECONDS} sec[s])"
        if is_step_allowed ${i_step}; then
          ${i_step} > ${ICM_DIR}/${i_step}.log
          echo $? >> ${ICM_DIR}/${i_step}.log
          echo "Step finished ${i_step} (dur: ${SECONDS} sec[s])"
        else
          echo "Step skipped (reason:$?) ${i_step}"
        fi
      done
runcmd:
  - bash -x /root/infra-config-minimal.sh
