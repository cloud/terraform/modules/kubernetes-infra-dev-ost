###############################################################
# Define networking                                           #
# Security group rules are in separate file secgroup_rules.tf #
###############################################################

resource "openstack_networking_network_v2" "network_default" {
  count          = var.internal_network_creation_enable ? 1 : 0
  name           = "${var.infra_name}_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_default" {
  count           = var.internal_subnet_creation_enable ? 1 : 0
  name            = "${var.infra_name}_subnet"
  network_id      = openstack_networking_network_v2.network_default[0].id
  cidr            = var.internal_network_cidr
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

data "openstack_networking_network_v2" "external_network" {
  name = var.public_external_network
}

data "openstack_networking_router_v2" "router_available" {
  count = var.router_creation_enable ? 0 : 1
}
resource "openstack_networking_router_v2" "router_default" {
  count               = var.router_creation_enable ? 1 : 0
  name                = "${var.infra_name}_router"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.external_network.id
}

resource "openstack_networking_router_interface_v2" "router_default_interface" {
  count     = var.router_creation_enable ? 1 : 0
  router_id = openstack_networking_router_v2.router_default[0].id
  subnet_id = openstack_networking_subnet_v2.subnet_default[0].id
}

resource "openstack_networking_router_interface_v2" "router_available_interface" {
  count     = var.router_creation_enable ? 0 : 1
  router_id = data.openstack_networking_router_v2.router_available[0].id
  subnet_id = openstack_networking_subnet_v2.subnet_default[0].id
}

# Ports
resource "openstack_networking_port_v2" "bastion_port" {
  name               = "${var.infra_name}-${var.bastion_name}"
  network_id         = openstack_networking_network_v2.network_default[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default[0].id
    ip_address = format("%s.%d", regex("[0-9]+\\.[0-9]+\\.[0-9]+", var.internal_network_cidr), 10)
  }
}

resource "openstack_networking_port_v2" "vip_port" {
  count              = var.kube_fip_create_port && var.kube_fip ? 1 : 0
  name               = "${var.infra_name}-vip"
  network_id         = openstack_networking_network_v2.network_default[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet_default[0].id
    ip_address = var.kube_vip
  }
}
resource "openstack_networking_port_v2" "worker_ports" {

  for_each = { for inst in local.all_instances : inst.name => inst }

  name               = each.value.name
  network_id         = openstack_networking_network_v2.network_default[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default[0].id
    ip_address = each.value.ip_address
  }
  allowed_address_pairs {
    ip_address = var.kube_vip
  }
  allowed_address_pairs {
    ip_address = "10.233.0.0/18"
  }
  allowed_address_pairs {
    ip_address = "10.233.64.0/18"
  }
}

resource "openstack_networking_port_v2" "control_ports" {
  count              = var.control_nodes_count
  name               = "${var.infra_name}-control-${count.index + 1}"
  network_id         = openstack_networking_network_v2.network_default[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default[0].id
    ip_address = format("%s.%d", regex("[0-9]+\\.[0-9]+\\.[0-9]+", var.internal_network_cidr), 11 + count.index)
  }
  allowed_address_pairs {
    ip_address = var.kube_vip
  }
  allowed_address_pairs {
    ip_address = "10.233.0.0/18"
  }
  allowed_address_pairs {
    ip_address = "10.233.64.0/18"
  }
}

# Floating IPs (only for bastion node)
resource "openstack_networking_floatingip_v2" "bastion_fip" {
  pool = var.public_external_network
}

resource "openstack_networking_floatingip_associate_v2" "bastion_fip_associate" {
  floating_ip = openstack_networking_floatingip_v2.bastion_fip.address
  port_id     = openstack_networking_port_v2.bastion_port.id
}

# Floating VIP IPs
resource "openstack_networking_floatingip_v2" "vip_fip" {
  count = var.kube_fip ? 1 : 0
  pool  = var.public_external_network
}

resource "openstack_networking_floatingip_associate_v2" "res_vip_fip_associate" {
  count       = var.kube_fip_create_port && var.kube_fip ? 1 : 0
  floating_ip = openstack_networking_floatingip_v2.vip_fip[count.index].address
  port_id     = openstack_networking_port_v2.vip_port[count.index].id
}
