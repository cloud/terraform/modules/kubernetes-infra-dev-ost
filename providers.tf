terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.4.1"
    }
  }
}
