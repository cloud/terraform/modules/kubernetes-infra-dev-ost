variable "infra_name" {
  description = "Infrastructure (profile) name. Used as a name prefix. Must match [a-zA-Z0-9-]+ regexp."
  default     = "general-tf-demo"
}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "internal_network_cidr" {
  description = "Internal network address, use CIDR notation"
  default     = "10.0.0.0/24"
}

variable "public_external_network" {
  description = "Cloud public external network pool"
  default     = "public-cesnet-195-113-167-GROUP"
}

variable "router_creation_enable" {
  description = "Create dedicated router instance. true/false ~ create new / reuse existing personal router"
  default     = true
}

variable "internal_network_creation_enable" {
  description = "Create dedicated internal network. true/false ~ create new / reuse existing personal network"
  default     = true
}

variable "internal_network_name" {
  description = "Internal network name. Either dedicated new network or existing personal network name"
  default     = "<var.infra_name>_network"
}

variable "internal_subnet_creation_enable" {
  description = "Create dedicated subnet instance. true/false ~ create new / reuse existing personal subnet"
  default     = true
}

variable "internal_subnet_name" {
  description = "Internal network subnet name. Either dedicated new subnet or existing personal subnet name"
  default     = "<var.infra_name>_subnet"
}

####################
# bastion settings #
####################
variable "bastion_name" {
  description = "Name of the bastion VM. Must match [a-zA-Z0-9-]+ regexp."
  default     = "bastion-server"
}

variable "bastion_flavor" {
  default = "standard.small"
}

variable "bastion_image" {
  description = "Bastion OS: Image name"
  default     = "ubuntu-jammy-x86_64"
}

variable "bastion_ssh_user_name" {
  default = "ubuntu"
}

#########################
# control nodes settings #
#########################
variable "control_nodes_count" {
  default = 3
}

variable "control_nodes_name" {
  description = "Name of the nodes. Must match [a-zA-Z0-9-]+ regexp."
  default     = "control"
}

variable "control_nodes_flavor" {
  default = "hpc.8core-32ram-ssd-ephem"
}

variable "control_nodes_volume_size" {
  description = "The size of the volume to create (in gigabytes) for root filesystem. "
  default     = "30"
}

variable "control_nodes_extra_volume_size" {
  description = "The size of the volume to create (in gigabytes) for root filesystem. "
  default     = "200"
}

#########################
# worker nodes settings #
#########################
variable "worker_nodes" {
  type = list(object({
    name        = string
    flavor      = string
    count       = number
    volume_size = number
  }))
}

variable "nodes_image" {
  description = "Image used for both control and worker servers"
  default     = "ubuntu-jammy-x86_64"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

#########################
# kube-vip nodes settings #
#########################

variable "kube_vip" {
  description = "Internal IP for kube-vip to access kube-api"
  default     = "10.0.0.5"
}

variable "kube_fip" {
  description = "Allocate floating IP for kubespray"
  type        = bool
  default     = false
}

variable "kube_fip_create_port" {
  description = "True if you want floating IP address for kube-vip to access kube-api"
  type        = bool
  default     = false
}

#########################
# control node volumes
#########################

variable "control_node_volumes_count" {
  description = "Number of volumes added to control nodes (0 to disable attaching volumes)"
  default     = ""
}
