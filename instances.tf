data "openstack_images_image_v2" "nodes_image" {
  name = var.nodes_image
}

####################
# Define bastion   #
####################

resource "openstack_compute_instance_v2" "bastion" {
  name            = "${var.infra_name}-${var.bastion_name}"
  image_name      = var.bastion_image
  flavor_name     = var.bastion_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.infra_name}-${var.bastion_name}.local\n${file("${path.cwd}/bastion-cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default[0].id
    port = openstack_networking_port_v2.bastion_port.id
  }
}

###############################
# Define kubernetes instances #
###############################

resource "openstack_compute_instance_v2" "control_nodes" {
  count           = var.control_nodes_count
  name            = "${var.infra_name}-${var.control_nodes_name}-${count.index + 1}"
  image_name      = var.nodes_image
  flavor_name     = var.control_nodes_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.infra_name}-${var.control_nodes_name}-${count.index + 1}.local\n${file("${path.cwd}/nodes-cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default[0].id
    port = element(openstack_networking_port_v2.control_ports.*.id, count.index)
  }

  block_device {
    uuid                  = data.openstack_images_image_v2.nodes_image.id
    source_type           = "image"
    volume_size           = var.control_nodes_volume_size
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }
}

resource "openstack_compute_instance_v2" "worker_nodes" {

  for_each = { for inst in local.all_instances : inst.name => inst }

  name            = each.value.name
  image_name      = var.nodes_image
  flavor_name     = each.value.flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.infra_name}-${each.value.name}.local\n${file("${path.cwd}/nodes-cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default[0].id
    port = openstack_networking_port_v2.worker_ports[each.value.name].id
  }

  block_device {
    uuid                  = data.openstack_images_image_v2.nodes_image.id
    source_type           = "image"
    volume_size           = each.value.volume_size
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }
}
